<?if( !defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true ) die();?>
<!DOCTYPE html>
<html>
	<head>
		<title><?$APPLICATION->ShowTitle()?></title>
		<?$APPLICATION->ShowHead()?>
		
		<?CJSCore::Init(array("jquery"));?>
	</head>
	<body>
		<div id="panel"><?$APPLICATION->ShowPanel();?></div>
		