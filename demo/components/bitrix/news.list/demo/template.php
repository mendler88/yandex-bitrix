<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>

<div class="item-list">
	<?foreach( $arResult["ITEMS"] as $arItem ){
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));?>
		
		<div class="item">
			<?if( !empty( $arItem["PROPERTIES"]["MAP"]["VALUE"] ) ){?>
				<a href="#" class="js-map" data-id="<?=$arItem["ID"]?>">
			<?}?>
			<?=$arItem["NAME"]?>
			<?if( !empty( $arItem["PROPERTIES"]["MAP"]["VALUE"] ) ){?>
				</a>
			<?}?>
		</div>
		
		<?if( !empty( $arItem["PROPERTIES"]["MAP"]["VALUE"] ) ){
			$coords = explode(',', $arItem["PROPERTIES"]["MAP"]["VALUE"]);
			$city_coords[0] += $coords[0];
			$city_coords[1] += $coords[1];
			$count++;
			
			$text = '<div class="title"><a href="'.$arItem["DETAIL_PAGE_URL"].'">'.$arItem["NAME"].'</a></div>';
			
			$arPlacemarks[] = array(
				"LON" => $coords[1],
				"LAT" => $coords[0],
				"TEXT" => $arItem["NAME"],
				"DETAIL_TEXT" => $text,
				"ITEM_ID" => $arItem["ID"]
			);
		}
	}
	$city_coords[0] /= $count;
	$city_coords[1] /= $count;?>
</div>
<div class="map">
	<span class="maps-arrow top-arrow"><img src="<?=$this->__folder.'/images/top-arrow.png'?>" alt="Вверх" /></span>
	<span class="maps-arrow right-arrow"><img src="<?=$this->__folder.'/images/right-arrow.png'?>" alt="Вправо" /></span>
	<span class="maps-arrow bottom-arrow"><img src="<?=$this->__folder.'/images/bottom-arrow.png'?>" alt="Вниз" /></span>
	<span class="maps-arrow left-arrow"><img src="<?=$this->__folder.'/images/left-arrow.png'?>" alt="Влево" /></span>
	<?$APPLICATION->IncludeComponent(
		"bitrix:map.yandex.view",
		"demo",
		Array(
			"INIT_MAP_TYPE" => "MAP",
			"MAP_DATA" => serialize( array( "yandex_lat" => $city_coords[0], "yandex_lon" => $city_coords[1], "yandex_scale" => 12, "PLACEMARKS" => $arPlacemarks ) ),
			"MAP_WIDTH" => "800",
			"MAP_HEIGHT" => "600",
			"CONTROLS" => array("ZOOM","TYPECONTROL","SCALELINE"),
			"OPTIONS" => array("ENABLE_DBLCLICK_ZOOM","ENABLE_DRAGGING"),
			"MAP_ID" => ""
		)
	);?>
</div>

<script>
	$(document).ready(function(){
		$('.item a').mouseover(function(){
			for( i = 0; i < arObjects.PLACEMARKS.length; i++ ){
				if( $(this).data('id') == arObjects.PLACEMARKS[i].options.get('item_id') ){
					var pc = arObjects.PLACEMARKS[i].geometry.getCoordinates();
					var mc = map_global.getBounds();
					
					if( pc[0] < mc[0][0] ){
						$('.bottom-arrow').show();
					}
					if( pc[1] < mc[0][1] ){
						$('.left-arrow').show();
					}
					if( pc[0] > mc[1][0] ){
						$('.top-arrow').show();
					}
					if( pc[1] > mc[1][1] ){
						$('.right-arrow').show();
					}
					if( pc[0] > mc[0][0] && pc[1] > mc[0][1] && pc[0] < mc[1][0] && pc[1] < mc[1][1] ){
						arObjects.PLACEMARKS[i].events.fire('mouseenter', [ pc ]);
					}
				}
			}
		}).mouseout(function(){
			for( i = 0; i < arObjects.PLACEMARKS.length; i++ ){
				if( $(this).data('id') == arObjects.PLACEMARKS[i].options.get('item_id') ){
					var pc = arObjects.PLACEMARKS[i].geometry.getCoordinates();
					var mc = map_global.getBounds();
					
					if( pc[0] < mc[0][0] || pc[1] < mc[0][1] || pc[0] > mc[1][0] || pc[1] > mc[1][1] ){
						$('.maps-arrow').hide();
					}else{
						arObjects.PLACEMARKS[i].events.fire('mouseleave', [ pc ]);
					}
				}
			}
		})
	})
</script>