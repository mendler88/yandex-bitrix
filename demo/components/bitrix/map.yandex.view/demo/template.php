<?if( !defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true ) die();?>

<?if( $arParams['BX_EDITOR_RENDER_MODE'] == 'Y' ){?>
	<img src="/bitrix/components/bitrix/map.yandex.view/templates/.default/images/screenshot.png" border="0" />
<?}else{
	
	$imarker = array();
	$imarker["PATH"] = $this->__folder.'/images/item-marker.png';
	$size = getimagesize( $_SERVER["DOCUMENT_ROOT"].$imarker["PATH"] );
	$imarker["WIDTH"] = $size[0];
	$imarker["HEIGHT"] = $size[1];
	
	$ihmarker = array();
	$ihmarker["PATH"] = $this->__folder.'/images/item-hover-marker.png';
	$size = getimagesize( $_SERVER["DOCUMENT_ROOT"].$ihmarker["PATH"] );
	$ihmarker["WIDTH"] = $size[0];
	$ihmarker["HEIGHT"] = $size[1];
	
	$cmarker = array();
	$cmarker["PATH"] = $this->__folder.'/images/cluster-marker.png';
	$size = getimagesize( $_SERVER["DOCUMENT_ROOT"].$cmarker["PATH"] );
	$cmarker["WIDTH"] = $size[0];
	$cmarker["HEIGHT"] = $size[1];
	
	$chmarker = array();
	$chmarker["PATH"] = $this->__folder.'/images/cluster-hover-marker.png';
	$size = getimagesize( $_SERVER["DOCUMENT_ROOT"].$cmarker["PATH"] );
	$chmarker["WIDTH"] = $size[0];
	$chmarker["HEIGHT"] = $size[1];
	
	$arTransParams = array(
		'KEY' => $arParams['KEY'],
		'INIT_MAP_TYPE' => $arParams['INIT_MAP_TYPE'],
		'INIT_MAP_LON' => $arResult['POSITION']['yandex_lon'],
		'INIT_MAP_LAT' => $arResult['POSITION']['yandex_lat'],
		'INIT_MAP_SCALE' => $arResult['POSITION']['yandex_scale'],
		'MAP_WIDTH' => $arParams['MAP_WIDTH'],
		'MAP_HEIGHT' => $arParams['MAP_HEIGHT'],
		'CONTROLS' => $arParams['CONTROLS'],
		'OPTIONS' => $arParams['OPTIONS'],
		'MAP_ID' => $arParams['MAP_ID'],
		'LOCALE' => $arParams['LOCALE'],
		'ONMAPREADY' => 'BX_SetPlacemarks_'.$arParams['MAP_ID'],
	);
	
	if( $arParams['DEV_MODE'] == 'Y' ){
		$arTransParams['DEV_MODE'] = 'Y';
		if( $arParams['WAIT_FOR_EVENT'] )
			$arTransParams['WAIT_FOR_EVENT'] = $arParams['WAIT_FOR_EVENT'];
	}?>
	<script type="text/javascript">
		var arObjects;
		var map_global;
		var clusterer;
		var clusterMarker = [{ href: '<?=$cmarker["PATH"]?>', size: [<?=$cmarker["WIDTH"]?>, <?=$cmarker["HEIGHT"]?>], offset: [-<?=$cmarker["WIDTH"]?>, -<?=$cmarker["HEIGHT"]?>] }];
		var clusterHoverMarker = [{ href: '<?=$chmarker["PATH"]?>', size: [<?=$chmarker["WIDTH"]?>, <?=$chmarker["HEIGHT"]?>], offset: [-<?=$chmarker["WIDTH"]?>, -<?=$chmarker["HEIGHT"]?>] }];
		
		function BX_SetPlacemarks_<?=$arParams['MAP_ID']?>( map ){
			arObjects = { PLACEMARKS:[], POLYLINES:[] };
			map_global = map;
			
			var MyIconContentLayout = ymaps.templateLayoutFactory.createClass('<div style="color: black; font-size: 15px;">$[properties.geoObjects.length]</div>');
			clusterer = new ymaps.Clusterer({
				clusterIcons: clusterMarker,
				clusterIconContentLayout: MyIconContentLayout,
				zoomMargin: 50,
				showInAlphabeticalOrder: true
			});
			
			<?if( is_array( $arResult['POSITION']['PLACEMARKS'] ) && ( $cnt = count( $arResult['POSITION']['PLACEMARKS'] ) ) ){
				for( $i = 0; $i < $cnt; $i++ ){?>
					arObjects.PLACEMARKS[arObjects.PLACEMARKS.length] = BX_YMapAddPlacemark( map, <?=CUtil::PhpToJsObject($arResult['POSITION']['PLACEMARKS'][$i])?> );
				<?}
			}?>
			
			clusterer.events.add('mouseenter', function(e){
				e.get('target').options.set('icons', clusterHoverMarker);
				e.get('target').options.set('zIndex', 999);
			});
			
			clusterer.events.add('mouseleave', function(e){
				e.get('target').options.set('icons', clusterMarker);
				e.get('target').options.set('zIndex', 1);
			});
			
			map.geoObjects.add( clusterer );
			
			<?if( $arParams['ONMAPREADY'] ){?>
				if( window.<?=$arParams['ONMAPREADY']?> ){
					window.<?=$arParams['ONMAPREADY']?>( map, arObjects );
				}
			<?}?>
		}
	</script>
	<div class="bx-yandex-view-layout">
		<div class="bx-yandex-view-map">
			<?$APPLICATION->IncludeComponent( 'bitrix:map.yandex.system', '.default', $arTransParams, false, array( 'HIDE_ICONS' => 'Y' ) );?>
		</div>
	</div>
	
	<script>
		window.BX_YMapAddPlacemark = function( map, arPlacemark ){
			if( null == map )
				return false;
			
			if( !arPlacemark.LAT || !arPlacemark.LON )
				return false;
			
			var properties = {};
			
			var option = { iconImageHref: '<?=$imarker["PATH"]?>', iconImageSize: [<?=$imarker["WIDTH"]?>, <?=$imarker["HEIGHT"]?>], iconImageOffset: [-<?=$imarker["WIDTH"] / 2?>, -<?=$imarker["HEIGHT"]?>], item_id: arPlacemark.ITEM_ID };
			var optionHover = { iconImageHref: '<?=$ihmarker["PATH"]?>', iconImageSize: [<?=$ihmarker["WIDTH"]?>, <?=$ihmarker["HEIGHT"]?>], iconImageOffset: [-<?=$ihmarker["WIDTH"] / 2?>, -<?=$ihmarker["HEIGHT"]?>], item_id: arPlacemark.ITEM_ID };
			
			if( arPlacemark.DETAIL_TEXT.length ){
				properties.balloonContent = arPlacemark.DETAIL_TEXT.replace(/\n/g, '<br />');
			}
			
			var obPlacemark = new ymaps.Placemark(
				[arPlacemark.LAT, arPlacemark.LON],
				properties,
				option
			);
			
			obPlacemark.events.add('mouseenter', function(e){
				var cl = clusterer.getObjectState( e.get('target') );
				if( cl.isClustered == true ){
					cl.cluster.events.fire('mouseenter', [cl.cluster.geometry.getCoordinates()]);
				}else{
					e.get('target').options.set('iconImageHref', optionHover.iconImageHref);
				}
			});
			
			obPlacemark.events.add('mouseleave', function(e){
				var cl = clusterer.getObjectState( e.get('target') );
				if( cl.isClustered == true ){
					cl.cluster.events.fire('mouseleave', [cl.cluster.geometry.getCoordinates()]);
				}else{
					e.get('target').options.set('iconImageHref', option.iconImageHref);
				}
			});
			
			clusterer.add( obPlacemark );
			
			return obPlacemark;
		}
	</script>
<?}?>